﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    interface ISsak
    {
        string ssak();
    }

    interface IPies : ISsak
    {
        string hau();
    }

    interface IKot : ISsak
    {
        string miau();
    }

    interface IRobot
    {
        string Laser();
        string Plazma();
    }
}
