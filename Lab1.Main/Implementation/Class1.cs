﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Pies : IPies
    {
        string ISsak.ssak()
        {
            return "pies Ssak";
        }
        string IPies.hau()
        {
            return "hau hau";
        }
       
    }

    class Kot : IKot
    {
        string ISsak.ssak()
        {
            return "kot Ssak";
        }
        string IKot.miau()
        {
            return "miau miau";
        }
    }
    class cyborg : IKot, IRobot
    {
        string ISsak.ssak()
        {
            return "Nie ssak";
        }
        string IKot.miau()
        {
            return "miau miau";
        }
        string IRobot.Laser()
        {
            return "strzel laserem";
        }
        string IRobot.Plazma()
        {
            return "strzel plazmą";
        }
    }
}
