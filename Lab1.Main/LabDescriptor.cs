﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ISsak);
        
        public static Type ISub1 = typeof(IKot);
        public static Type Impl1 = typeof(Kot);
        
        public static Type ISub2 = typeof(IPies);
        public static Type Impl2 = typeof(Pies);
        
        
        public static string baseMethod = "ssak";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "miau";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "hau";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "ISsak";
        public static string collectionConsumerMethod = "ssak";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3 = typeof(cyborg);

        public static string otherCommonMethod = "ssak";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
